"use client";

import React from "react";
import SectionHeading from "./section-heading";
import { motion } from "framer-motion";
import { useSectionInView } from "src/lib/hooks";

export default function About() {
  const { ref } = useSectionInView("About");

  return (
    <motion.section
      ref={ref}
      className="mb-28 max-w-[45rem] text-center leading-8 sm:mb-40 scroll-mt-28"
      initial={{ opacity: 0, y: 100 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: 0.175 }}
      id="about"
    >
      <SectionHeading>About me</SectionHeading>
      <p className="mb-3">
        As a passionate and dedicated programmer, my professional journey is
        driven by an insatiable curiosity and a relentless pursuit of personal
        and technical excellence. My career in software development is marked by
        a continuous quest for innovation and improvement, aiming to create
        solutions that not only meet user needs
      </p>
    </motion.section>
  );
}
