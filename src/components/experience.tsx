"use client";

import React from "react";
import SectionHeading from "./section-heading";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import { useSectionInView } from "src/lib/hooks";
import { useTheme } from "@/context/theme-context";

export default function Experience() {
  const { ref } = useSectionInView("Experience");
  const { theme } = useTheme();

  return (
    <section id="experience" ref={ref} className="scroll-mt-28 mb-28 sm:mb-40">
      <SectionHeading>My experience</SectionHeading>
          <h3 className="font-semibold capitalize">Full Stack Developer</h3>
          <p className="font-normal !mt-0">
            Road Accident Victims Protection Company Limited
          </p>
          <p className="!mt-1 !font-normal text-gray-700 dark:text-white/75">
            • Developed a Web application using .Net that improved user
            experience.
            <br />
            • Developed an API using .NET Core to fetch data and automatically
            check for fraud.
            <br />
            • Design a database and optimize performance.
            <br />
            • Developed a Web application using .Net that improved user
            experience.
            <br />
            • Developed an API using .NET Core to fetch data and automatically
            check for fraud.
            <br />• Design a database and optimize performance.
          </p>
    </section>
  );
}
